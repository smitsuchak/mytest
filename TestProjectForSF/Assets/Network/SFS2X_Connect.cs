﻿using UnityEngine;
using System.Collections;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;

public class SFS2X_Connect : MonoBehaviour {
	
	public string ServerIP = "127.0.0.1";			//localhost
	public int ServerPort = 9933;					// port
	public string ZoneName = "SmitZone";
	public string UserName ="";

	//For config file
	public string ConfigFile ="Network/sfs-config.xml";
	public bool UseConfigFile = false;
	public string RoomName = "lobby";

	SmartFox sfs;
	
	void Start()
	{
		//instantiate SmartFox obj
		sfs = new SmartFox();
		// Hold down the events until we ask for
		sfs.ThreadSafeMode = true;


		sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
		sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
		sfs.AddEventListener(SFSEvent.LOGIN_ERROR, OnLoginError);

		//Events for Config
		sfs.AddEventListener(SFSEvent.CONFIG_LOAD_SUCCESS, OnConfigLoad);
		sfs.AddEventListener(SFSEvent.CONFIG_LOAD_FAILURE, OnConfigFail);

		sfs.AddEventListener(SFSEvent.ROOM_JOIN, OnJoinRoom);
		sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, OnJoinRoomError);

		// Public Message Listener
		sfs.AddEventListener(SFSEvent.PUBLIC_MESSAGE, OnPublicMessage);

		//Check if you want to conenct via config file or code.
		if (UseConfigFile)
		{
			sfs.LoadConfig(Application.dataPath + "/" + ConfigFile);
		} else {
			sfs.Connect(ServerIP, ServerPort);
		}


		//Extension Response Listnere , Basic implementation is there in SendXtensionRequest.cs
		// This listener will listen to any extension response send by server
		sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);


	}

	//Click event for button 
	public void SendXtensionRequestToServer(){

		//Object to send the request to the server with Command SumNumbers 
		ISFSObject objOut = new SFSObject();
		objOut.PutInt("Num1", 2);
		objOut.PutInt("Num2", 5);

		//creating new extension request and sending the SumNumbers command with obj data
		//Extension Request can also be specific room and use UDP but we will use simple overload
		sfs.Send(new ExtensionRequest("SumNumbers", objOut));

	}
	
	//Extension Response to receive information after sending cmd SumNumbers
	void OnExtensionResponse(BaseEvent e)
	{
		string cmd = (string)e.Params["cmd"];
		ISFSObject objIn = (SFSObject)e.Params["params"];

		//Since this listener will listen to any response send by user so we need to check which response is there 
		//using commands and handle respectively.
		if (cmd == "SumNumbers")
		{
			Debug.Log("Sum: " + objIn.GetInt("Sum"));
		}
	}



	// call back for public message
	void OnPublicMessage(BaseEvent e)
	{
		Room room = (Room)e.Params["room"];
		User sender = (User)e.Params["sender"];
		Debug.Log("[" + room.Name + "] " + sender.Name + ": " + e.Params["message"]);
	}


	void OnConfigLoad(BaseEvent e)
	{
		Debug.Log("Config File Loaded");
		sfs.Connect(sfs.Config.Host, sfs.Config.Port);
	}
	
	void OnConfigFail(BaseEvent e)
	{
		Debug.Log("Failed to load Config File");
	}

	void OnJoinRoom(BaseEvent e)
	{
		Debug.Log("Joined Room: " + e.Params["room"]);

		//Sending a public message to everyone after joining a room
		sfs.Send(new PublicMessageRequest("Welcome to The Lobby !"));
	}
	
	void OnJoinRoomError(BaseEvent e)
	{
		Debug.Log("Join Room Error (" + e.Params["errorCode"] + "): " + e.Params["errorMessage"]);
	}


	void OnLogin(BaseEvent e){
	
		Debug.Log ("Logged In" + e.Params["user"]);
		sfs.Send(new JoinRoomRequest(RoomName));
	}

	void OnLoginError(BaseEvent e){

		Debug.Log ("Login Error" + e.Params["errorCode"] + "msg" + e.Params["errorMessage"]);
	
	}
	
	void OnConnection(BaseEvent e)
	{
		if ((bool)e.Params["success"])
		{
			Debug.Log("Successfully Connected");

			if (UseConfigFile)
				ZoneName = sfs.Config.Zone;

			sfs.Send(new LoginRequest(UserName,"",ZoneName));
		} else {
			Debug.Log("Connection Failed");
		}
	}
	
	void Update()
	{
		//will ask for events every update
		sfs.ProcessEvents();
	}
	
	void OnApplicationQuit()
	{
		//If Application Quits disconnect the connection
		if (sfs.IsConnected)
			sfs.Disconnect();
	}
}