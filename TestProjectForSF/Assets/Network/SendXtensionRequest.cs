﻿using UnityEngine;
using System.Collections;
using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Entities.Data;

/*
 * SFS is not instantiated in this class so please do not use this class 
 * this is just to know how to send the extension request to Server.
 * 
 * Even  though if you want to use this get the sfs instance from where it has got instantiated, maybe singleton 
 * class will be better option
 * 
 * 
 * */
public class SendXtensionRequest : MonoBehaviour {

	SmartFox sfs;

	// Use this for initialization
	void Start () {
		if(sfs!=null)
		sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
	}


	void SendXtensionRequestToServer(){

		ISFSObject objOut = new SFSObject();
		objOut.PutInt("Num1", 2);
		objOut.PutInt("Num2", 5);
		
		sfs.Send(new ExtensionRequest("SumNumbers", objOut));
	
	}


	void OnExtensionResponse(BaseEvent e)
	{
		string cmd = (string)e.Params["cmd"];
		ISFSObject objIn = (SFSObject)e.Params["params"];
		
		if (cmd == "SumNumbers")
		{
			Debug.Log("Sum: " + objIn.GetInt("NumC"));
		}
	}
}
