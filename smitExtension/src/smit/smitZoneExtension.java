/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smit;

import com.smartfoxserver.v2.extensions.SFSExtension;

/**
 *
 * @author byofstudios
 */
public class smitZoneExtension extends SFSExtension{

    @Override
    public void init() {
        trace("smit  Extension for SFS2X started,");
    
        //Listening for command call SumNumbers sent by client and will be handle by SumNumberhandle class
        addRequestHandler("SumNumbers", SumNumbersHandler.class);
    }
    
}
