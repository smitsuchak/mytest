/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smit;

import com.smartfoxserver.v2.entities.User;
import com.smartfoxserver.v2.entities.data.ISFSObject;
import com.smartfoxserver.v2.entities.data.SFSObject;
import com.smartfoxserver.v2.extensions.BaseClientRequestHandler;

/**
 *
 * @author byofstudios
 */
public class SumNumbersHandler extends BaseClientRequestHandler{

    @Override
    public void handleClientRequest(User user, ISFSObject isfso) {
          
        //trying to add two numbers
        //lets take two numbers
        int num1 = isfso.getInt("Num1");
        int num2 = isfso.getInt("Num2");
        
        //Lets create object that will send information back to the client
        ISFSObject objOut = new SFSObject();
        objOut.putInt("Sum", num1 + num2);
        
        
        send("SumNumbers",objOut,user);
    
    }
    
}
